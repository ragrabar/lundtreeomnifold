#!/usr/bin/env python3

# This file is part of MultiFold unfolding software adapted to Lund Trees for the  ATLAS Z + bbbar analysis
# it is somewhat adapted from https://github.com/hep-lbdl/OmniFold and uses the OmniFold method of unfolding (1911.09107) and LundNet GNN classifiers (2012.08526)
# R. Grabarczyk, 2023
import os
import numpy as np
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
import tqdm
from lundhelpers import train, evaluate, LundWeightedDataset, collate_wrapper_weighted_tree, set_iteration_parameters
from LundNet import LundNet
import yaml
import argparse
import dgl
from random import random
import copy

def omnifold(particle_level_sim_graphs, detector_level_sim_graphs, pseudo_data, config):
    # for each iteration defines the omega and nu weights (of length = len(particle_level_sim_graphs))
    # Create a list of training configurations, according to the desired number of loops over configs
    # For now, I see no reason to have different architecture classifiers for STEP 1 and STEP 2, so by hand they are identical.
    list_of_infos = []
#    target_model_path_reco, target_model_path_truth = None,None
    for training_info in config:
        ### for each training_info
        for i in range(training_info['n_runs_over_this_iteration']):
            list_of_infos.append(copy.deepcopy(training_info))
    counter = len(list_of_infos)
    # we have a list of k training infos for k total iterations. Now we need to set the parameters for how to save
    # models and what source models to use (usually coming from the previous iteration)
    list_of_infos = set_iteration_parameters(list_of_infos)
    print(list_of_infos)

    weights = np.empty(shape=(counter, 2, len(particle_level_sim_graphs)))
    # TODO: [x] TRUTH VS RECO MODELS: ADAPT CONFIG TO BE ABLE TO READ PREVIOUS RECO AND PREVIOUS TRUTH MODELS
    #           IN LUNDLIKELIHOODAPPROXIMATOR
    # set the initial weights if given...
    if list_of_infos[0]['initial_weights_path'] is not None:
        #omega_weights = np.ones(len(particle_level_sim_graphs))
        #nu_weights = np.ones(len(detector_level_sim_graphs))
        print("Not implemented yet!!! Initial weights can only be 1s")
        return 0
    else:
        omega_weights = np.ones(len(particle_level_sim_graphs))
        nu_weights = np.ones(len(detector_level_sim_graphs))
    ones = np.ones(len(pseudo_data))
    # OmniFold iterations:
    #   STEP 1:
    #   ω_n = ν_(n-1) * L[(1, Data),(ν_(n-1), Detector_sim)] 
    #   STEP 2:
    #   ν_n = ν_(n-1) * L[(ω_n, Generator_sim), (ν_(n-1), Generator_sim)]
    for i in range(counter):
        print("\n ################################## ITERATION " + str(i + 1) + ": ##################################\n")
        print("\n ################# STEP 1: #################\n")

        omega_weights = nu_weights \
        * LundLikelihoodApproximation(ones, pseudo_data, nu_weights, detector_level_sim_graphs, list_of_infos[i], step = 1)
        
        print("\n ################# STEP 2: #################\n")

        nu_weights = nu_weights \
        * LundLikelihoodApproximation(omega_weights, particle_level_sim_graphs, nu_weights, particle_level_sim_graphs, list_of_infos[i], step = 2)
        
        weights[i,:1,:] = omega_weights
        weights[i,1:2,:] = nu_weights
    
    return np.array(weights)
    
def LundLikelihoodApproximation(weights1, graphs1, weights2, graphs2, config, step = None):
    # The constructor trains a model using the information in training_info.
    #  
    # For STEP 2 put graphs1 = MCgraphs, graphs2 = MCgraphs
    if step is None:
        print('Please specify the step')
        return 0

    if len(weights1) != len(graphs1) or len(weights2) != len(graphs2):
        raise Exception("Error: length of weights_i must equal to graphs_i for i = 1,2." \
        "Instead got len(weights1) = " + str(len(weights1)) + " len(weights2) = " + str(len(weights2)) + \
        " len(graphs1) = " + str(len(graphs1)) + " len(graphs2) = " + str(len(graphs2)) + ".")

    labels1 = [1. for i in range(len(graphs1))]
    labels2 = [0. for i in range(len(graphs2))] 

    graph1_data = LundWeightedDataset(graphs1, labels1, weights1)
    graph2_data = LundWeightedDataset(graphs2, labels2, weights2)

    all_graph_data = torch.utils.data.ConcatDataset([graph1_data, graph2_data])
    split_number1 = int(np.floor(config['train_fraction'] * len(all_graph_data)))
    split_number2 = len(all_graph_data) - split_number1                                                                        
    all_train_data, val_data = torch.utils.data.random_split(all_graph_data, [split_number1, split_number2])#[float(training_info['train_fraction']), 1. - float(training_info['train_fraction'])])
    split_number3 = int(np.floor(config['dataset_fraction'] * len(all_train_data)))
    train_data, _ = torch.utils.data.random_split(all_train_data, [split_number3, len(all_train_data) - split_number3])

    config['number of training samples'] = split_number3
    config['number of validation samples'] = split_number2

    # prepare dataloaders for training and validation
    train_loader = DataLoader(train_data,num_workers=int(config['num_workers']),batch_size=int(config['batch_size']),
                              collate_fn=collate_wrapper_weighted_tree, shuffle=True, drop_last=True, pin_memory=False)
    val_loader = DataLoader(val_data,num_workers=int(config['num_workers']),batch_size=int(config['batch_size']),
                                collate_fn=collate_wrapper_weighted_tree, shuffle=False, drop_last=True, pin_memory=False)
    dev = torch.device('cuda:0' if 'cuda' in config['device'] else 'cpu')

    model_type = config['model']['model_type']
    if 'LundNet' or 'lundnet' in model_type:
        Net = LundNet
        conv_params = [[32, 32], [32, 32], [64, 64], [64, 64], [128, 128], [128, 128]]
        fc_params = [(256, 0.1)]
        use_fusion = True
        if '5' in model_type:
            input_dims = 5
        if '3' in model_type:
            input_dims = 3
    model = Net(input_dims=input_dims, num_classes=2,
            conv_params=conv_params,
            fc_params=fc_params,
            use_fusion=use_fusion)

    model = model.to(dev)

    if step == 1:
        load_model_path = config['model'].get('source_model_path_reco')
    if step == 2:
        load_model_path = config['model'].get('source_model_path_truth')

    if load_model_path is not None:
        print('Loading model %s for finetuning' % load_model_path)
        model.load_state_dict(torch.load(load_model_path))

    model = model.to(dev)

    # specify optimizer from training_info:
    # FOR NOW, optimizer and scheduler are set by hand.
    # I don't think they will make a large difference.
    optimizer_config = config['optimizer']
    scheduler_config = config['scheduler']
    opt = torch.optim.Adam(model.parameters(), lr=float(optimizer_config['lr']))
    # learning rate
    scheduler = torch.optim.lr_scheduler.StepLR(opt, float(scheduler_config['step_size']), gamma=float(scheduler_config['gamma']))
    # TRAINING THE MODEL:

    # initialise list of accuracy values
    best_valid_acc = 0
    acc_list = []
    config.update({'training_time': 0, 
                          'training_time_per_sample': 0})
    if step == 1:
        save_model_path = config['model'].get('target_model_path_reco')
    if step == 2:
        save_model_path = config['model'].get('target_model_path_truth')
    # training loop
    for epoch in range(int(config['num_epochs'])):
        train(model, opt, scheduler, train_loader, dev, config) # make training_info contain everything
        print('Epoch #%d Validating' % epoch)
        valid_acc = evaluate(model, val_loader, dev)
        acc_list += [valid_acc]
        if valid_acc > best_valid_acc:
            best_valid_acc = valid_acc
 #           if not os.path.exists(best_model_path):
 #                       os.makedirs(best_model_path)
            torch.save(model.state_dict(), save_model_path)
        print('Current validation acc: %.5f (best: %.5f)' % (valid_acc, best_valid_acc))
    
    del all_graph_data, train_data, train_loader, val_data, val_loader, opt, scheduler, all_train_data, graph1_data
    
    config['validation_accuracies'] = acc_list
    # save the training info file
    with open(config['info_file'], 'w') as f:
        for k in config:
            f.write('%s: %s\n' % (k, config[k]))
    print('Evaluating scores...')
    # here we need to load the best saved model (highest validation accuracy)
    graph2_loader = DataLoader(graph2_data,num_workers=config['num_workers'],batch_size=config['batch_size'],
                                collate_fn=collate_wrapper_weighted_tree, shuffle=False, drop_last=False, pin_memory=False)
#    if not save_model_path.endswith('.pt'):
#        save_model_path = save_model_path + '_state.pt'
    print('(Loading model %s for eval)' % save_model_path)
    model.load_state_dict(torch.load(save_model_path)) #may need: map_location = dev, see lundnet.py
    scores = evaluate(model, graph2_loader, dev, return_scores = True)

    del graph2_loader, graph2_data, model

    print('Evaluating likelihoods...')
    likelihoods = np.nan_to_num(scores[:,1]/(1. - scores[:,1]))
    print(likelihoods)
    return likelihoods

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Parser for input file')
    parser.add_argument('--config', type=str, help='Path to the config yaml file')
    parser.add_argument('--detectorfile', type = str, help='Path to the detector level graphs')
    parser.add_argument('--generatorfile', type = str, help='Path to the detector level graphs')
    parser.add_argument('--datafile', type = str, help='Path to the data graphs')
    args = parser.parse_args()

    with open(args.config, "rb") as yaml_file:
        config = yaml.safe_load(yaml_file)
    # this has to happen before omnifold to have a nice output info file
#    with open(config[0]['info_file'], 'w') as f: 
#        f.write('# This file was generated using the multifold.py script. \n')
#        f.write('detector simulation file path: %s\n' % args.detectorfile)
#        f.write('generator simulation file path: %s\n' % args.generatorfile)
        
    detector_graphs, _ = dgl.load_graphs(args.detectorfile)
    generator_graphs, _ = dgl.load_graphs(args.generatorfile)
    data_graphs, _ = dgl.load_graphs(args.datafile)
    # third argument should be pseudodata/data
    weights = omnifold(generator_graphs, detector_graphs, data_graphs, config)
    np.save('weights.npy', weights)


    




    

    
